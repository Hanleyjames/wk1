//install express server
const express = require('express');
const path = require('path');
const app = express();
//serve static files from dist directory
app.use(express.static(__dirname + '/dist/dtoor'));
app.get('/*', function(req,res){
res.sendFile(path.join(__dirname+'/dist/dtoor/index.html'));
});
//start app listening to default heroku port
app.listen(process.env.PORT || 8080);
