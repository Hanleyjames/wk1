# Dtoor

This serves as a placeholder for a single page Crash and Burn two day project for a single page app.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

|completeness|task|
|----|----|
|Done|Create Header|
|Done|Create Footer|
|Done|Link Social Media|
|Done|Integrate MailChimp Form|
|Not Done|Implement Logo|
|Not Done|Integrate Grid|
|Not Done|Make Flowchart|
