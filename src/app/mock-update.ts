import { Updates } from './models/updates.model';

export const UPDATES: Updates[] = [
  new Updates('0', 'Coming soon', 'laurem ipsum something and so on'),
  new Updates('1', 'It\'s almost here', 'Doggo time')
];
