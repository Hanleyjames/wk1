import { Faq } from './models/faq.model';

export const FAQS: Faq[] = [
  new Faq('Why are there no images of your new phone model?', 'We are saving images of the phone for a week before the launch since we would like to keep our design uniquely our own. However if you\'re interested in knowing more and being among the exclusive first to catch a glimpse of how this model looks, feel free to subscribe to our YouTube channel.'),
  new Faq('When is it being launched?', '2018'),
  new Faq('What does \'non-rectangular\' mean?', 'Anything that goes against convention(legally). Be different. Think outside the rectangle.'),
  new Faq('So, why a circle?', 'Rectangles are man-made shapes. They don \' occur naturally. Circles are ubiquitous in nature. They are cool. They are different. Plus they fit in your pocket more easily.'),
  new Faq('What are the specifications?', 'They are LTE Android phones, but for the same reasons mentioned in the first answer, we cannot disclose any more details about the phone at this time.'),
  new Faq('How can I find out more?', 'Sign up to recieve lanch alerts and the occasional "insider" update.')
];
