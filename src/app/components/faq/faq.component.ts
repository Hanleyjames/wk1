import { Component, OnInit } from '@angular/core';
import { FAQS } from './../../mock-faq';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
  faqs = FAQS;
  constructor() { }

  ngOnInit() {
  }

}
