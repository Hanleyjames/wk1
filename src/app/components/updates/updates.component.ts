import { Component, OnInit } from '@angular/core';
import { UPDATES } from './../../mock-update';
@Component({
  selector: 'app-updates',
  templateUrl: './updates.component.html',
  styleUrls: ['./updates.component.css']
})
export class UpdatesComponent implements OnInit {
  updates = UPDATES;
  constructor() { }

  ngOnInit() {
  }

}
